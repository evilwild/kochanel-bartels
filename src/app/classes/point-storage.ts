import { Point } from './point';

export class PointStorage {

  private _storage: Point[];
  public get storage(): Point[] {
    return this._storage;
  }
  public set storage(v: Point[]) {
    this._storage = v;
  }

  /**
   *
   */
  constructor() {
    this.storage = new Array<Point>();
  }

  addPoint(p: Point): void {
    if (p == null) {
      throw new Error('No point specified');
    }
    const oldStorageLength: number = this.storage.length;
    this.storage.push(p);
    const newStorageLength: number = this.storage.length;
    if (oldStorageLength > newStorageLength) {
      throw new Error('Error while adding point');
    }
  }

  deletePoint(p: Point): void {
    const oldStorageLength: number = this.storage.length;
    this.storage = this.storage.filter((sP: Point) => {
      return sP.ID !== p.ID;
    });
    const newStorageLength: number = this.storage.length;
    if (oldStorageLength < newStorageLength) {
      throw new Error('Error while deleting point');
    }
  }

  length() {
    return this.storage.length;
  }

  findPoint(p: Point): boolean {
    let index = -1;
    let i = 0;
    this.storage.forEach(sP => {
      if (sP === p) {
        index = i;
      }
      i++;
    });
    if (index > -1) {
      return true;
    } else {
      return false;
    }
  }
}
