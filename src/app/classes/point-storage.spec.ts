import { PointStorage } from './point-storage';
import { Point } from './point';

describe('PointStorage', () => {
  it('should create an instance', () => {
    expect(new PointStorage()).toBeTruthy();
  });
  it('should throw error while adding point', () => {
    const ps = new PointStorage();
    expect(ps.addPoint(null)).toThrow(new Error('No point specified'));
  });
  it('should add point', () => {
    const ps = new PointStorage();
    expect(ps.addPoint(new Point(1, 2))).toBeUndefined();
  });
  it('should delete point', () => {
    const ps = new PointStorage();
    const p = new Point(1, 2);
    ps.addPoint(p);
    expect(ps.deletePoint(p)).toBeUndefined();
  });
  it('should find point', () => {
    const ps = new PointStorage();
    ps.addPoint(new Point(
      Math.floor(Math.random() * 100),
      Math.floor(Math.random() * 100)));
    ps.addPoint(new Point(
      Math.floor(Math.random() * 100),
      Math.floor(Math.random() * 100)));
    const p = new Point(1, 4);
    ps.addPoint(p);
    ps.addPoint(new Point(
      Math.floor(Math.random() * 100),
      Math.floor(Math.random() * 100)));
    expect(ps.findPoint(p)).toBeTrue();
  });
  it('should fail finding point', () => {
    const ps = new PointStorage();
    ps.addPoint(new Point(
      Math.floor(Math.random() * 100),
      Math.floor(Math.random() * 100)));
    ps.addPoint(new Point(
      Math.floor(Math.random() * 100),
      Math.floor(Math.random() * 100)));
    const p = new Point(1, 4);
    ps.addPoint(new Point(
      Math.floor(Math.random() * 100),
      Math.floor(Math.random() * 100)));
    expect(ps.findPoint(p)).toBeFalse();
  });
});
